package com.hw.db.controllers;


import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.threadController;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class threadControllerTests {
    private Thread thisThread;
    private int id = 0;
    private String slug = "thisSlug";

    private User thisUser;
    private String name = "UserName";
    private String email = "tratrata@mivesemssoboy.kota";
    threadController controller;
    private Post thisPost;

    LinkedList<Post> postsStub;

    @BeforeEach
    @DisplayName("thread test creation test")
    void createdThreadTest() {
        thisThread = new Thread("thisName",
                new Timestamp(0),
                "thisForum",
                "thisMessage",
                slug,
                "thisTitle",
                15)
        ;

        thisUser = new User(name, email, "Full Name", "nothing interesting");
        controller = new threadController();
        thisPost = new Post(thisUser.getNickname(), new Timestamp(1), "thisForum", "thisMessage", 0, 0, false);
        postsStub = new LinkedList<>();
        postsStub.add(thisPost);
    }

    @Test
    @DisplayName("Check ID or Slug")
    void checkedIDorSlug() {
        try (
                MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(id)).thenReturn(thisThread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thisThread);


            assertEquals(controller.CheckIdOrSlug(Integer.toString(id)), thisThread, "Correctly checked by ID");
            assertEquals(controller.CheckIdOrSlug("Non-existing id"), null, "Correctly checked that Non-existing id is not present");


            assertEquals(controller.CheckIdOrSlug(slug), thisThread, "Correctly checked by slug");
            assertEquals(controller.CheckIdOrSlug("123"), null, "Correctly checked that Non-existing slug is not present");

        }
    }

    @Test
    @DisplayName("Create Post")
    void createdPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thisThread);
                userMock.when(() -> UserDAO.Info(name)).thenReturn(thisUser);


                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postsStub), controller.createPost(slug, postsStub), "Correctly created a ne wpost");
            }
        }
    }


    @Test
    @DisplayName("Get Posts")
    void gotPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thisThread);
            threadMock.when(() -> ThreadDAO.treeSort(0, 1, 0, false)).thenReturn(postsStub);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()), controller.Posts(slug, 1, 0, null, false), "Correctly recieved all posts");
        }
    }

    @Test
    @DisplayName("Change Thread")
    void changedThread() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            String newSlug = "NewSlug";
            int newId = 33;
            Thread newThread = new Thread("newMan", new Timestamp(66), "newForum", "emptyMessage", newSlug, "title", 3823);
            newThread.setId(newId);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(newSlug)).thenReturn(newThread);
            threadMock.when(() -> ThreadDAO.getThreadById(newId)).thenReturn(newThread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), controller.change(newSlug, newThread), "correctly changed the thread");
        }
    }


    @Test
    @DisplayName("Get Thread Info")
    void gotThreadInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thisThread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thisThread), controller.info(slug), "Correctly got Thread Info");
        }
    }

    @Test
    @DisplayName("Create Vote")
    void createdVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thisThread);
                userMock.when(() -> UserDAO.Info(name)).thenReturn(thisUser);
                Vote vote = new Vote(name, 0);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thisThread), controller.createVote(slug, vote), "Correcctly created new vote");

            }
        }
    }


}
